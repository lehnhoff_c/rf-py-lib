# RF Python Library #

### What is this repository for? ###

This project offers a python library to interact with 433 Mhz radio frequency transmitters and receivers.

* based on https://github.com/ninjablocks/433Utils
* uses boost python to generate a python wrapper over the c++ library

### How do I get set up? ###
#### Hardware ####
* This project was developed and tested for a Raspberry PI 3 Model B
* The follwing radio frequency transmitters and receivers were used:
	* TODO: add link
	* TODO: add link


#### Python 3.9 ####
* This project was based on python 3.9, however it is assumed that any python version > 3.5 will be sufficient 
* Python 3.9 can be setup on the Rasperry Pi using the follwing commands:

		$ wget https://www.python.org/ftp/python/3.9.0/Python-3.9.0.tar.xz
		$ tar xf Python-3.9.0.tar.xz
		$ ./configure --enable-shared --enable-optimizations --prefix=/usr/local/opt/python3.9 LDFLAGS=-Wl,-rpath=/usr/local/opt/python3.9/lib
		$ make -j -l 4
		$ make altinstall

#### CMake ####
* CMake is used as C++ build and dependency management tool for the target python library
* CMake can be installed as follows:

		$ git clone -b v3.17.4 https://gitlab.kitware.com/cmake/cmake.git cmake
		$ cd cmake/
		$ ./bootstrap --system-curl
		$ sudo make install
		$ cmake --version

#### WiringPi ####
* The WiringPi library is required to enable GPIO interaction from within C++
* WiringPi can be installed as follows:
* TODO: add instructions

#### Boost Pyhton ####	
* Boost Python is used to wrap the C++ library to enable usage from within pyhton
* Boost Python can be setup by invoking the following commands:
	
		$ mkdir boost
		$ cd boost
		$ wget http://sourceforge.net/projects/boost/files/boost/1.74.0/boost_1_74_0.tar.bz2
		$ tar xvfo boost_1_74_0.tar.bz2
		$ ./bootstrap.sh --with-libraries=python
		$ sudo ./b2 install

### How do I build the library ###
In order to build the library the following commands must be invoked in the root directory of this project

		$ cmake .
		$ make
		
If the build is successful you will find the rfpylib.so under a newly created bin dir. One can now run the test_rfpylib.py to validate that the python bindigns work as expected.