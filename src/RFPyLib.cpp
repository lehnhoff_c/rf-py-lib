#include <boost/python.hpp>
#include "RFDeviceController.h"

using namespace boost::python;

BOOST_PYTHON_MODULE(rfpylib)
{
  class_< RFDeviceController >("RFDeviceController", init<>())
    // .def("start_sniffing", &RFDeviceController::startSniffing)
    .def("start_sniffing", +[](RFDeviceController& self, int pin, boost::python::object callback)
    {
      self.startSniffing(pin, callback);
    })
    .def("stop_sniffing", &RFDeviceController::stopSniffing);
}
