﻿#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <mutex>
#include <thread>
#include <chrono>
#include <functional>
#include <wiringPi.h>
#include <boost/python.hpp>
#include "RFDeviceController.h"

std::mutex _sniffingMutex;

RFDeviceController::RFDeviceController()
{
    _rcSwitch = RCSwitch();

    if(wiringPiSetupPhys() == -1) {
        throw "WiringPiSetup failed...";
    }
    Py_Initialize();
}


void RFDeviceController::startSniffing(const int inputPin, std::function<void(int, int)> callback)
{
    std::cout << "[RfPyLib] Attempting to sniff for signals on pin " << inputPin << std::endl;
    _rcSwitch.enableReceive(inputPin);
    std::thread t(&RFDeviceController::sniffSignal, this, callback);
    t.detach();
}

void RFDeviceController::sniffSignal(std::function<void(int, int)> callback)
{
    while (true) {
        std::unique_lock<std::mutex> sniffingLock(_sniffingMutex);
        if (_stopSniffing) {
            _stopSniffing = false;
            sniffingLock.unlock();
            break;
        }
        sniffingLock.unlock();

        if (_rcSwitch.available()) {
            int signalValue = _rcSwitch.getReceivedValue();
            int signalProtocol = _rcSwitch.getReceivedProtocol();

            if (signalValue == 0) {
                std::cout << "[RfPyLib] Error! Failed to decocde received value..." << std::endl;
            }
            else {
                PyGILState_STATE state = PyGILState_Ensure();
                std::cout << "[RfPyLib] Received signal '" << signalValue << "' (Protocol: " << signalProtocol << ")" << std::endl;
                callback(signalProtocol, signalValue);
                PyGILState_Release(state);
            }
            _rcSwitch.resetAvailable();
        }
        std::this_thread::sleep_for(std::chrono::microseconds(100));
    }
    std::cout << "[RfPyLib] Stopped sniffing..." << std::endl;
}

void RFDeviceController::stopSniffing()
{
    std::unique_lock<std::mutex> sniffingLock(_sniffingMutex);
    _stopSniffing = true;
    _rcSwitch.disableReceive();
}

void RFDeviceController::sendSignal(const int outputPin, const int payload)
{
    _rcSwitch.enableTransmit(outputPin);
    _rcSwitch.send(payload, 23);
}

void onSignalReceivedCallback(int signalValue, int signalProtocol)
{
    std::cout << "[RfPyLib] Received signal from callback: " << signalValue << " (Protocol: " << signalProtocol << ")" << std::endl;
}

int main(int argc, char *argv[]) {

    RFDeviceController controller = RFDeviceController();
    controller.startSniffing(13, onSignalReceivedCallback);

    while(true) {
        std::cout << "[RfPyLib] Still running..." << std::endl;
        usleep(1000000);
    }

}
