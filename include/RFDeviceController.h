#pragma once
#include "RCSwitch.h"

class RFDeviceController {
	public:
		RFDeviceController(); // physical pin (https://de.pinout.xyz/)
		void startSniffing(const int inputPin, std::function<void(int, int)> callback);
		void stopSniffing();
		void sendSignal(const int outputPin, const int payload);

	private:
		RCSwitch _rcSwitch;		
		void sniffSignal(std::function<void(int, int)> callback);
		bool _stopSniffing = false;
};
