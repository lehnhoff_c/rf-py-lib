import sys
import time
sys.path.append('./bin')
from rfpylib import RFDeviceController

def on_signal_received(protcol, payload):
    print(f'[Python] Received signal payload: {payload} (Protocol: {protcol})')
    
if __name__ == "__main__":
    controller = RFDeviceController()
    print('[Python] Starting to sniff on pin 13')
    controller.start_sniffing(13, on_signal_received)
    count = 0
    while count < 10:
        # print('[Python] Waiting another 5 seconds...');
        time.sleep(5)
        count+=1
    controller.stop_sniffing()